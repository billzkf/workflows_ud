=====
Statistical Workflows
=====

This repository contains a collection of "workflows" for performing statistical analysis. Each folder in the repository contains a "workflow" for performing a type of analysis, as well as example code for some of the most common statistical packages (stata, R, spss, SAS).


Editing .rst files
======

Math
-----

Note that MathJax is supported when using reStructuredText (.rst) format on for README files on bitbucket. You can write latex syntax math (view source) :math:`a_1^2 + a_2^2 = a_3^2`.

Tables
------

== == ==
A  B  C
== == ==
1  2  4
3  4  6
== == ==

or this way

+---+---+
| A | B |
+===+===+
| 1 | 2 |
+---+---+
| 3 | 4 |
+---+---+

Code
------

.. code-block:: R

  add <- function(a, b) {
    return (a+b);
  }