requiredPackages <- c("car","ggplot2","moments","sandwich")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}
require(moments)
require(car)
require(sandwich)
require(ggplot2)

hSize <- 720
vSize <- 360

set.seed(12345)
obs <- 1000
data <- data.frame(id = c(1:obs))

data$age <- round(20 + 60 * runif(obs))
data$iq <- round(100 + 16 * rnorm(obs) - 0.15 * (data$age - 50))

#2.1 Frequencies
iqTable <- table(data$iq)
prop.table(iqTable)
ageTable <- table(data$age)
prop.table(ageTable)

#2.2 Univariate plots
png(filename = "linear_iqHist.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = iq)) +
  geom_histogram(binwidth = 5)
dev.off()

png(filename = "linear_iqBox.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = 1, y = iq)) +
  geom_boxplot()
dev.off()

png(filename = "linear_qqplot.png", units = "px", width = hSize, height = vSize)
qqnorm(data$iq)
dev.off()

png(filename = "linear_iqHist.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = age)) +
  geom_histogram(binwidth = 5)
dev.off()

png(filename = "linear_iqBox.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = 1, y = age)) +
  geom_boxplot()
dev.off()

png(filename = "linear_qqplot.png", units = "px", width = hSize, height = vSize)
qqnorm(data$age)
dev.off()

#2.3 Testing Normality of Distributions
skewness(data$iq)
kurtosis(data$iq)
skewness(data$age)
kurtosis(data$age)

#2.4 scatterplot
png(filename = "linear_iqAgeScatter.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = age, y = iq)) + 
  geom_point()
dev.off()


#2.5 lowess
png(filename = "linear_lowess.png", units = "px", width = hSize, height = vSize)
scatter.smooth(data$age, data$iq)
dev.off()

#2.6 model
ols <- lm(iq ~ age, data = data)
summary(ols)
robustSE <- coeftest(ols, vcov = vcovHC(mod, type="HC1"))

#2.7 model evaluation
png(filename = "linear_smooth1.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = age, y = iq)) + 
  geom_point() +
  geom_smooth(method=lm)
dev.off()

#2.8 regression diagnostics
data$yhat <- predict(ols)
data$iqResid <- data$iq - data$yhat
#Are the residuals normally distributed?
png(filename="linear_residualHist.png", units = "px", width = hSize, height = vSize)
ggplot(data, aes(x = iqResid)) +
  geom_histogram()
dev.off()

png(filename="linear_qqResid.png", units = "px", width = hSize, height = vSize)
qqnorm(data$iqResid)
dev.off()

skewness(data$iqResid)
kurtosis(data$iqResid)
shapiro.test(data$iqResid)
#TODO rvfplots

# influence measures, res vs lev, res vs fit plots, het. test
plot(ols)
inf <- influence.measures(ols)
summary(inf)
bptest(ols, data = data)

#TODO more regression diagnostics