Preamble
========

Numerous tests are typically available to test whether the observed
distribution of your response variable is consistent with the
assumptions (e.g., Gaussian). When you find or strongly suspect that you
are violating distributional assumptions, you should consider
nonparametric approaches to analysis. Many nonparametric tests are
simply a parametric test performed on ranked data. Because ranks are by
definition sequential, for a given number of observations, several
characteristics of the ranks (e.g., sum, and therefore mean) are known.
Many parametric statistics are generally quite robust to violations of
distributional assumptions, and so nonparametric and parametric tests
can often yield the same conclusions. There is also a broad area of
so-called “robust statistics” that are not as sensitive to violations of
distributional assumptions. The techniques listed below are not
exhaustive, but rather represent the most commonly encountered and
required nonparametric tests. One confusing aspect of nonparametric
statistics is that the same test can have multiple names and different
tests can often have similar names. For this reason, the content below
is organized by the nature of your analysis (i.e., what you are trying
to accomplish) rather than specific test names.

Assessing Normality
===================

There are numerous ways to evaluate the assumption of normality for a
univariate distribution. Each is based, in some way, on comparing the
observed distribution with what would be expected under assumptions of
normality. For example, the first 4 moments (mean, variance, skewness,
and kurtosis) of a standard normal variate are 0, 1, 0, and 3 \[many
programs subtract this expected value so that the expected kurtosis is
also 0\]). Ralph D’Agostino presented a **Skewness-Kurtosis test** to
evaluate normality via skewness and kurtosis. This can be obtained in
Stata using for $n > 8$.

Another commonly applied test for normality is the **Shapiro-Wilk
test**, which is useful for $4 \leq n\leq 2000$. It can be obtained in
stata using .

Similarly, the **Shapiro-Francia test** is useful for
$5 \leq n \leq 5000$ and can be obtained using .

A generalized test for one distribution against another can be found in
the **Kolmogorov-Smirnov** equality-of-distributions test. This test
will compare data with expectations under an arbitrary distribution or
formula. For example, to test normality of a distribution in Stata, type
. This test can also be used to compare distributions across two groups.
For example, .

Some statistical tests require (or assume) multivariate normality.
**Mardia’s test** is perhaps the most widely known and applied. It can
be obtained in Stata via .

Two Independent Groups
======================

You know that you have independent data if an individual can only appear
in one group, and where you only have one observation for each
individuals. Common examples include treatment or control, men or women.

Mann-Whitney U Test (a.k.a. Wilcoxon Rank Sum Test)
---------------------------------------------------

The Mann-Whitney U test is the nonparametric analog of an independent
samples t-test. It tests whether the mean ranks are equal across groups.
To obtain this test in Stata, use .

$k$ Independent Groups
======================

Kruskal-Wallis Test
-------------------

A test of across :math:`k` groups can be obtained in Stata using . A test of
is available in Stata using . Again, if your sample size is large or
your computer slow, you may wish to omit the option.

Two Dependent (Paired) Groups
=============================

Paired data are, for example, pre and post-test scores. They are not
independent. You know you have paired data if you have more than one
rating for each individual.

Sign Test
---------

The sign test is one of the simplest nonparametric tests. It is almost
never reported in a scientific paper, but is handy to calculate quickly
during a first pass through the data. To perform a sign tests in Stata,
type .

Wilcoxon Signed-Rank Test
-------------------------

The Wilcoxon signed rank tests is the equivalent of a paired t-test for
non-normal data. it is essentially like running a paired t-test on
ranked data instead of raw data. To perform this test in Stata, use .

Correlation
===========

Spearman’s rank-order correlation coefficient $(\rho)$ is the most
commonly used nonparametric measure of association. It uses exactly the
same formula as Pearson’s product-moment correlation. However, instead
of being calculated with the raw variables, it uses rank-ordered data.
In Stata, Spearman’s rho can be obtained by typing .

**Stata Syntax: Distributions**

    /*****************************************************************
    * Sample Nonparametric Tests Syntax
    * PH 8012, Spring 2015
    * Adam Davey
    *****************************************************************/

    #delimit;
    clear all;
    capture log close;
    log using "mylog.log", replace;

    * Below simulates some data for the workflow;
    set seed 12345;
    set obs 1000;

    gen gp2 = mod(_n,2);
    gen gp3 = mod(_n,3);

    gen x0 = rnormal();
    gen x1 = 10*runiform() + 5*gp2;
    gen x2 = 10*rpoisson(5) + 3*gp3;

    * Distributional Tests;
    sktest x0 x1 x2;
    swilk x0 x1 x2;
    sfrancia x0 x1 x2;
    summ x1;
    ksmirnov x1 = normal(x1-r(mean)/r(sd));
    ksmirnov x1, by(gp2);
    mvtest normality x0 x1 x2, bivariate univariate stats(all);

    * Two Independent Groups (Mann-Whitney);
    ranksum x1, by(gp2);

    * k Independent Groups;
    kwallis x2, by(gp3);

    * Two Dependent Groups;
    * Sign test;
    signtest x0=x1;

    * Wilcoxon Signed-Rank Tests;
    signrank x1=x2;

    * Spearman's rho;
    spearman x0 x1 x2, stats(rho obs p);

    log close;

    * Convert text output to PDF;
    * Note: A two step procedure is needed under Linux;
    translate mylog.log mylog.ps, replace;
    !ps2pdf mylog.ps;
