set.seed(12345)

obs <- 1000
randData <- data.frame(id = c(1:obs))

#Uniform
randData$uniform <- runif(obs, 0, 1)

#Bernoulli
randData$coinToss <- rbinom(obs, 1, 0.5)
dbinom(1, 1, 0.5)
pbinom(1, 1, 0.5)
qbinom(0, 1, 0.5) #?

#Binomial
randData$cointosses <- rbinom(obs, 20, 0.5)
dbinom(10, 20, 0.5)
pbinom(10, 20, 0.5)
qbinom(10 / 20, 20, 0.5)
qbinom(0.5, 10, 0.5, lower.tail = FALSE)

#Poisson
randData$helminths <- rpois(obs, 5)
dpois(4, 5)
ppois(5, 5)
ppois(5, 5, lower.tail = FALSE) #?

#Normal
iq <- rnorm(obs, 100, 16)
dnorm(0)
dnorm(100, 100, 16)
pnorm(0)
qnorm(0.5)
qnorm(0.975)

#Chi-squared
randData$chi10 <- rchisq(obs, 10)
dchisq(10, 10)
pchisq(3.84, 1)
qchisq(0.95, 1)
qchisq(0.05, 1, lower.tail = FALSE)

#t
randData$t5 <- rt(obs, 5)
dt(0, 5)
pt(0, 5, lower.tail = FALSE)
pt(0, 5)
qt(0.975, 5)
qt(0.025, 5, lower.tail = FALSE)

#F
randData$f30_100 <- (rchisq(obs, 30) / 30) / (rchisq(obs, 100) / 100)
df(1, 30, 100)
pf(1, 30, 100)
pf(1, 30, 100, lower.tail = FALSE)
qf(0.95, 30, 100)
qf(0.05, 30, 100, lower.tail = FALSE)

summary(randData)