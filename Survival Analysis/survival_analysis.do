/*****************************************************************
* Sample Cox Proportional Hazards Regression Syntax
* PBHL 8012, Spring 2016
* Adam Davey
* Requires outreg2
* Type: findit outreg2 to install
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "mysurvival.log", replace;

* Below simulates some data for the workflow;
set seed 83633;
local lambdat = 0.2;
local gammat = 2;
local lambdac = 0.4;
local gammac = -1;
set obs 1000;
gen id = _n;
gen age = int(rnormal(50,10)) + 1;
gen time = int(10*((log(1-uniform()))/-`lambdat'*exp(-.05*age))^(1/`gammat'));
gen censor = int(10*((log(1-uniform()))/-`lambdac')^(1/`gammac'));
gen died = censor >= time;
replace time = min(time,censor);
drop censor;
replace age = round(age,5);
gen old = age>=65;

* Step 2.1 Frequencies / Summary Statistics;
* Recode 0 survival times to small value;
replace time = 0.01 if time==0;

* Declaring survival time data;
stset time, fail(died==1);
stsum;
stdescribe;

tab age, missing;
summarize age;

* Step 2.2 Univariate Plots;
histogram age, by(died);
graph export "hist_age_died.pdf", replace;
graph box age, by(died);
graph export "box_age_died.pdf", replace;
qnorm age if died==0;
graph export "qnorm_age_died0.pdf", replace;
qnorm age if died==1;
graph export "qnorm_age_died1.pdf", replace;

sts graph, survival ci censored(number);
graph export "km_survival.pdf", replace;
sts graph, failure ci censored(number);
graph export "km_failure.pdf", replace;
sts graph, hazard ci;
graph export "km_hazard.pdf", replace;

* Step 2.3 Testing Normality of Distributions;
* NA;

* Step 2.4 Scatterplot;
* Totally useless, right?;
twoway (scatter time age, sort mlabel(died));
graph export "scatter_time_age.pdf", replace;

* Step 2.5 Lowess;
lowess died time, logit;
graph export "lowess_died_time.pdf", replace;
* Martingale Residuals;
* Estimate model without predictors;
stcox, mgale(mg0) efron estimate;
* Use them in place of Lowess plot above;
lowess mg0 age;
graph export "lowess_martingales.pdf", replace;


* Step 2.6 Estimate Model;
* First, estimate and store model with normal theory SEs;
stcox age;
stcox age, nohr;
stcox age, vce(robust);
stcox age, breslow;
stcox age, efron;
* Save the preferred model;
estimates store coxph;
stcox age, exactm;
stcox age, exactp;
* Restore estimates from default model;
estimates restore coxph;
* Try the model below;
*stcox i.age, efron;
estat concordance;

* Step 2.7 Evaluate Model;
* Please see workflow text for what to look for;
* Here's a margins plot;
margins, at(age=(20(5)80));
marginsplot, xlabel(20(5)80) recast(line) recastci(rarea);
graph export "margins_age.pdf", replace;

* Step 2.8 Regression Diagnostics;
* Specification;
linktest;

* Proportional Hazard tests and plots;
estat phtest, detail;
estat phtest, plot(age);
graph export "schoenfeld_age.pdf", replace;
stphplot, by(old);
graph export "phtest_old.pdf", replace;
stcoxkm, by(old);
graph export "coxkm_old.pdf", replace;

* Cox and Snell GOF;
stcox age, efron;
predict cs, csnell;
stset cs, fail(died==1);
sts gen cumhaz = na;
line cumhaz cs cs, sort ytitle("") legend(cols(1));
graph export "cox-snell_gof.pdf", replace;

* Outliers and Influential Points;
stcox age, efron esr(e*);
mkmat e*, matrix(esr);
mat V = e(V);
mat Inf = esr*V;
svmat Inf, names(s);
scatter s1 _t, yline(0) mlabel(id) msymbol(i);
graph export "inf_age.pdf", replace;

* Step 2.9 Tabling Results;
* Example 1 -- direct to Excel;
* This is different from other workflows;
* We have to reset and reestimate everything here before tabling;
stset time, fail(died==1);
stsum;
quietly: stcox age, efron;
putexcel set "survival.xls", sheet("Cox Proportional Hazards") replace;
putexcel F1=("Number of obs")          G1=(e(N));
local chi2type = e(chi2type);
putexcel F2=("`chi2type' Chi-square")  G2=(e(chi2));
putexcel F3=("df")                     G3=(e(df_m));
putexcel F4=("Prob > Chi-square")      G4=(max(0.0001,chi2tail(e(df_m),e(chi2))));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A6=matrix(a, names);
quietly: estat concordance;
putexcel F5=("Harrell's C")       G5=(r(C));

log close;

* Convert text output to PDF;
translate mysurvival.log mysurvival.pdf, replace;
