requiredPackages <- c("car","ggplot2","sandwich","ResourceSelection","lmtest","caret","xlsx")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}

require(car)
require(ggplot2)
require(sandwich)
require(lmtest)
require(ResourceSelection)
require(caret)
require(xlsx)


binaryLogRegWorkflow <- function(data, iv, dv, imWidth=720, imHeight=480){
  #create the Excel workbook
  binRegXL <- createWorkbook()
  descXL <- createSheet(wb=binRegXL, sheetName="Descriptives")
  regXL <- createSheet(wb=binRegXL, sheetName="Regression")
  
  obs <- nrow(data)
  data <- as.data.frame(data)
  #test dv to make sure it is an int
  #if(all(data[[dv]] == 1 || data[[dv]] == 0)){
  #   stop('The dependent variable needs to be a 1 or 0.')
  #}
  
  #create subsets with and without outcome for later
  dataPos <- subset(data, data[[dv]] == 1)
  dataNeg <- subset(data, data[[dv]] == 0)
  
  #2.1 Frequencies
  dvTable <- table(data[[dv]])
  prop.table(dvTable)
  
  ivTable <- table(data[[iv]])
  prop.table(ivTable)
  
  png(filename = "posHist.png", units = "px", width = imWidth, height = imHeight)
  ggplot(dataPos, aes_string(x=iv)) +
    geom_histogram()
  dev.off()
  
  png(filename = "negHist.png", units = "px", width = imWidth, height = imHeight)
  ggplot(dataNeg, aes_string(x=iv)) +
    geom_histogram()
  dev.off()
  
  png(filename = "boxplot.png", units = "px", width = imWidth, height = imHeight)
  ggplot(data, aes_string(x=iv, y=dv)) +
    geom_boxplot()
  dev.off()
  
  png(filename = "qqPos.png", units = "px", width = imWidth, height = imHeight)
  qqnorm(dataPos[[iv]])
  dev.off()
  
  png(filename = "qqNeg.png", units = "px", width = imWidth, height = imHeight)  
  qqnorm(dataNeg[[iv]])
  dev.off()
  
  #2.5
  png(filename = "lowess.png", units = "px", width = imWidth, height = imHeight)
  #scatter.smooth(data[[iv]], y = data[[dv]])
  dev.off()
  
  #2.6
  
  form <- as.formula(paste(dv,'~',iv))
  mod <- glm(form, family = "binomial",data=data)
  summary(mod)$coefficients
  confint(mod, level = 0.95)
  #odds ratio
  exp(coef(mod))
  #address heteroskedasticity
  robustSE <- coeftest(mod, vcov = vcovHC(mod, type="HC1"))
  summary(robustSE)
  
  #2.8 Regression Diagnostics
  data$yhat <- predict.glm(mod, data = data)
  data$phat <- predict.glm(mod, data = data, type = "response")
  data$predicted <- as.integer(data$phat >= 0.5)
  
  data$predicted <- factor(wfData$predicted,c(0,1))
  data$actual <- factor(wfData$genius)
  
  table(wfData$predicted,wfData$actual)
  
  sens <- sensitivity(wfData$predicted,wfData$actual)
  ppv <- posPredValue(wfData$predicted,wfData$actual)
  
  spec <- specificity(wfData$predicted,wfData$actual)
  nnv <- negPredValue(wfData$predicted,wfData$actual)
  
  
  #from Paul et al 2013
  checkGOF <- TRUE
  if(obs < 1000){
    nGroups <- 10
  } else if(obs <= 25000) {
    m <- sum(data[dv])
    nGroups <- round(max(10, min(m / 2, (obs - m) / 2,(2 + 8 * (obs / 1000) ^ 2))))
  } else {
    getGOF <- FALSE
    print(paste("Hosmer-Lemeshow not recommended when N = ", obs))
  }
  if(checkGOF){
    hlgof <- hoslem.test(data[[dv]], data$yhat, g = nGroups)
    print(hlgof)
    addDataFrame(dataPos, descXL)
  }
  
  saveWorkbook(binRegXL, "Simple_Binary_Logistic_Regression.xlsx")
}