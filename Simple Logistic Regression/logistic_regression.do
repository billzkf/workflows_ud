/*****************************************************************
* Sample Logistic Regression Syntax
* PBHL 8012, Spring 2016
* Adam Davey
* Requires outreg2
* Type: findit outreg2 to install
* Requires lrdrop1
* Type: findit lrdrop1 to install
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "mylogistic.log", replace;
*log using "mylog.smcl", replace;

* Below simulates some data for the workflow;
set seed 8675309;
set obs 1000;
* As an aside, genius is usually 140+ for IQ;
* This results in too few cases, so we will allow those Mensa morons;
* Just imagine air quotes around the variable name;
generate genius = _n <= 50;
generate age = int(20 + 60*runiform() - 7*genius + 7*0.05);
* The lines below are for in-class example;
generate old = age>=65;
generate older = age >=70;
generate oldest = age>=75;
gen id = _n;

* Step 2.1 Frequencies / Summary Statistics;
tab1 age genius, missing;
by genius, sort: summarize age;
* Alternatively, use syntax below for considerably more information;
*summarize iq age, detail;

* Step 2.2 Univariate Plots;
histogram age, by(genius);
graph export "hist_age.pdf", replace;
graph box age, by(genius);
graph export "box_age.pdf", replace;
qnorm age if genius==0;
graph export "qnorm_age0.pdf", replace;
qnorm age if genius==1;
graph export "qnorm_age1.pdf", replace;

* Step 2.3 Testing Normality of Distributions;
* NA;

* Step 2.4 Scatterplot;
* NA;

* Step 2.5 Lowess;
lowess genius age, logit;
graph export "lowess_genius_age.pdf", replace;

* Step 2.6 Estimate Model;
* First, estimate and store model with normal theory SEs;
logit genius age;
estimates store logitnorm;
logistic genius age;
estimates store logisticnorm;

* Then estimate and store model with robust SEs;
logit genius age, vce(robust);
estimates store logitrob;
logistic genius age, vce(robust);
estimates store logisticrob;

* Step 2.7 Evaluate Model;
* No additional analyses or syntax for this section, so here's a handy figure;
* Remember to check observed range for x-variable;
margins, at(age=(15(5)80));
marginsplot, xlabel(15(5)80) recast(line) recastci(rarea);
graph export "logit_margins_cis.pdf", replace;

* Step 2.8 Regression Diagnostics;
* Classification table (confusion matrix);
estat class;

* ROC curve with AUC statistic;
lroc;
graph export "roc.pdf", replace;

* Sensitivity-Specificity x phat;
lsens;
graph export "sensitivity_specificity.pdf", replace;

* Calculate number of groups based on Paul et al 2013 criteria;
quietly {;
local n = _N;
if `n' < 1000 {;
local numgps = 10;
noisily: estat gof, group(`numgps');
};
else if `n' >= 1000 & `n' <=25000 {;
quietly sum genius;
local m = r(sum);
local numgps = int(max(10,min(`m'/2,(`n'-`m')/2,(2 + 8*(`n'/1000)^2))));
noisily: estat gof, group(`numgps');
};
else if `n' > 25000 {;
local numgps = .;
noisily: di "Hosmer-Lemeshow not recommended when N = `n'";
};
};
lsens;
linktest;

* Various predicted values;
predict yhat, xb;
predict phat, pr;
predict spresid, rstandard;
predict devresid, deviance;
predict hhat, hat;
predict dfhat, dbeta;

* Various diagnostic plots;
scatter spresid phat, mlab(id) yline(0);
graph export "spresid_x_phat.pdf", replace;
scatter devresid phat, mlab(id) yline(0);
graph export "devresid_x_phat.pdf", replace;
scatter hhat id, mlab(id) yline(0);
graph export "hhat_x_id.pdf", replace;
scatter dfhat id, mlab(id) yline(0);
graph export "dfhat_x_id.pdf", replace;

* Step 2.9 Tabling Results;
* Example 1 -- direct to Excel;
quietly: estimates replay logitrob;
putexcel set "logistic.xls", sheet("Logit Robust SEs") replace;
putexcel F1=("Number of obs")          G1=(e(N));
local chi2type = e(chi2type);
putexcel F2=("`chi2type' Chi-square")  G2=(e(chi2));
putexcel F3=("df")                     G3=(e(df_m));
putexcel F4=("Prob > Chi-square")      G4=(e(p));
putexcel F5=("Pseudo R-squared")       G5=(e(r2_p));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A6=matrix(a, names);
quietly: estimates replay logitnorm;
quietly: estimates replay logisticrob;
putexcel set "logistic.xls", sheet("Logistic Robust SEs") modify;
putexcel F1=("Number of obs")          G1=(e(N));
local chi2type = e(chi2type);
putexcel F2=("`chi2type' Chi-square")  G2=(e(chi2));
putexcel F3=("df")                     G3=(e(df_m));
putexcel F4=("Prob > Chi-square")      G4=(e(p));
putexcel F5=("Pseudo R-squared")       G5=(e(r2_p));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A6=matrix(a, names);
quietly: estimates replay logitnorm;
putexcel set "logistic.xls", sheet("Logit Normal SEs") modify;
putexcel F1=("Number of obs")          G1=(e(N));
local chi2type = e(chi2type);
putexcel F2=("`chi2type' Chi-square")  G2=(e(chi2));
putexcel F3=("df")                     G3=(e(df_m));
putexcel F4=("Prob > Chi-square")      G4=(e(p));
putexcel F5=("Pseudo R-squared")       G5=(e(r2_p));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A6=matrix(a, names);
quietly: estimates replay logisticnorm;
putexcel set "logistic.xls", sheet("Logistic Normal SEs") modify;
putexcel F1=("Number of obs")          G1=(e(N));
local chi2type = e(chi2type);
putexcel F2=("`chi2type' Chi-square")  G2=(e(chi2));
putexcel F3=("df")                     G3=(e(df_m));
putexcel F4=("Prob > Chi-square")      G4=(e(p));
putexcel F5=("Pseudo R-squared")       G5=(e(r2_p));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A6=matrix(a, names);

* Example 2 -- also direct to Excel;
* Nicer table, but labels need to be changed;
outreg2 [logitrob logitnorm] using "logistic2",
 replace excel stats(coef ci pval)
 bdec(2) cdec(2) pdec(3) bracket(ci) noaster sideway;

log close;

* Convert text output to PDF;
translate mylogistic.log mylogistic.ps, replace;
!ps2pdf mylogistic.ps;
