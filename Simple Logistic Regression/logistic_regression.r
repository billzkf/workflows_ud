requiredPackages <- c("car","ggplot2","sandwich","lmtest","ROCR","caret")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}

require(car)
require(ggplot2)
require(sandwich)
require(lmtest)
require(ROCR)
require(caret)
set.seed(1944)

#plot size
hSize <- 720
vSize <- 360

#generate workflow data
obs <- 1000
wfData <- data.frame(id = c(1:obs))
wfData$genius <- as.integer(wfData$id <= 50)
wfData$age <- round(20 + 60 * runif(obs,0,1) - 7 * wfData$genius + 7 * 0.05)
wfData$old <- wfData$age >= 65
wfData$older <- wfData$age >= 70
wfData$oldest <- wfData$age >= 75

#or import stata data for exact check
#wfData <- read.csv("c:/dev/workflows/Simple Logistic Regression/from_stata.csv")

#2.1 Frequencies
genTable <- table(wfData$genius)
prop.table(genTable)
ageTable <- table(wfData$age)
prop.table(ageTable)

xtabs(genius ~ age, wfData)

#2.2 Univariate plots
geniuses <- subset(wfData, genius == 1)
nonGeniuses <- subset(wfData, genius == 0)

png(filename = "log_geniusHist.png", units = "px", width = hSize, height = vSize)
ggplot(geniuses, aes(x=age)) +
  geom_histogram(binwidth=10)
dev.off()


png(filename = "log_geniusBox.png", units = "px", width = hSize, height = vSize)
ggplot(wfData, aes(x=genius, y=age)) +
  geom_boxplot()
dev.off()

png(filename = "log_qqGenius.png", units = "px", width = hSize, height = vSize)
qqnorm(geniuses$age)
dev.off()

png(filename = "log_qqNonGenius.png", units = "px", width = hSize, height = vSize)
qqnorm(nonGeniuses$age)
dev.off()


#2.3

#2.4

#2.5
png(filename = "log_lowess.png", units = "px", width = hSize, height = vSize)
scatter.smooth(wfData$age, y = wfData$genius)
dev.off()

#2.6
mod <- glm(genius ~ age, family = "binomial", data = wfData)
summary(mod)$coefficients
confint(mod, level = 0.95)
#odds ratio
exp(coef(mod))
#address heteroskedasticity
robustSE <- coeftest(mod, vcov = vcovHC(mod, type="HC1"))
robustSE
#2.7

#2.8 Regression Diagnostics
wfData$yhat <- predict.glm(mod, data = wfData)
wfData$phat <- predict.glm(mod, data = wfData, type = "response")
wfData$predicted <- as.integer(wfData$phat >= 0.5)

wfData$predicted <- factor(wfData$predicted,c(0,1))
wfData$actual <- factor(wfData$genius)

table(wfData$predicted,wfData$actual,)

sens <- sensitivity(wfData$predicted,wfData$actual)
ppv <- posPredValue(wfData$predicted,wfData$actual)

spec <- specificity(wfData$predicted,wfData$actual)
nnv <- negPredValue(wfData$predicted,wfData$actual)

#from Paul et al 2013
checkGOF <- TRUE
if(obs < 1000){
  nGroups <- 10
} else if(obs <= 25000) {
  m <- sum(wfData$genius)
  nGroups <- round(max(10, min(m / 2, (obs - m) / 2,(2 + 8 * (obs / 1000) ^ 2))))
} else {
  getGOF <- FALSE
  print(paste("Hosmer-Lemeshow not recommended when N = ", obs))
}
if(checkGOF){
  hlgof <- hoslem.test(wfData$genius, wfData$yhat, g = nGroups)
  hlgof
}

#TODO Diagnostic Plots
