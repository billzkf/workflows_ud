#delimit;
clear all;
capture log close;
log using "mylogistic.log", replace;

* Below simulates some data for the workflow;
set seed 8675309;
set obs 1000;
* As an aside, genius is usually 140+ for IQ;
* This results in too few cases, so we will allow those Mensa morons;
* Just imagine air quotes around the variable name;
generate genius = _n <= 50;
generate age = int(20 + 60*runiform() - 7*genius + 7*0.05);
* The lines below are for in-class example;
generate old = age>=65;
generate older = age >=70;
generate oldest = age>=75;
gen id = _n;

*Define variables
local iv age;//insert variable name here
local dv genius;//insert dependent variable name here

*Label your variables for better file outputs
label variable `iv' "Age"; //insert name here
label variable `dv' "Gen"; //insert name here 

* Step 2.1 Frequencies / Summary Statistics;
tab1 `iv' `dv', missing;
by `dv', sort: summarize `iv';
* Alternatively, use 'detail' option for considerably more information;

* Step 2.2 Univariate Plots;
*histogram `iv', by `dv';

* Step 2.5 Lowess;
*lowess `dv' `iv', logit;
*graph export "lowess_genius_age.pdf", replace;

* Step 2.6 Estimate Model;
* First, estimate and store model with normal theory SEs;
logit `dv' `iv';
estimates store logitnorm;
logistic `dv' `iv';
estimates store logisticnorm;

* Then estimate and store model with robust SEs;
logit `dv' `iv', vce(robust);
estimates store logitrob;
logistic `dv' `iv', vce(robust);
estimates store logisticrob;

* Step 2.7 Evaluate Model;
* No additional analyses or syntax for this section, so here's a handy figure;
* Remember to check observed range for x-variable;
margins, at(`iv'=(15(5)80));
marginsplot, xlabel(15(5)80) recast(line) recastci(rarea);
graph export "logit_margins_cis.pdf", replace;
