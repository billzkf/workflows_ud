/*****************************************************************
* Sample Paper (Binary Outcome) Syntax
* PBHL 8012, Spring 2016
* Adam Davey
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "binary_paper.log", replace;

* Below simulates some data for the workflow;
set seed 12345;
set obs 1000;


log close;

* Convert text output to PDF;
* Note: A two step procedure is needed under Linux;
translate binary_paper.log binary_paper.ps, replace;
!ps2pdf binary_paper.ps;
